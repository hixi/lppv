#!/usr/bin/env python
import os, codecs
from subprocess import Popen, PIPE

COLORS = {
        'reset':'\033[0m',
        'black': '\033[30m',
        'red': '\033[31m',
        'green': '\033[32m',
        'brown': '\033[33m',
        'blue': '\033[34m',
        'magenta': '\033[35m',
        'cyan': '\033[36m',
        'lightgray': '\033[37m',
}

def main():
    CUR_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__)))
    VERSIONS_FILE = os.path.join(CUR_DIR, 'your_versions.txt')
    if os.path.exists(VERSIONS_FILE) and os.stat(VERSIONS_FILE).st_size > 0:
        for line in codecs.open(VERSIONS_FILE, 'r', encoding='utf-8').readlines():
            package, version = line.split('==')
            output, error = Popen(['yolk', '-V', package], stdout=PIPE, stderr=PIPE).communicate()
            # output , err = Popen(["duply", args.profile, "status"], stdout=PIPE, stderr=PIPE , env={'HOME': '/root', 'PATH': os.environ['PATH']}).communicate()
            pak, newest_version = output.split('\n')[0].split()
            kwargs = {
                    'pak': pak,
                    'newest_version': newest_version,
                    'version': version,
                    'all_ok': COLORS['green'],
                    'watch_out': COLORS['red'],
                    'reset': COLORS['reset'],
            }
            if newest_version.strip() != version.strip():
                print "The Package {watch_out}{pak}{reset} is out of date. The newest Version is {all_ok}{newest_version}{reset} and you have installed {watch_out}{version}{reset}".format(**kwargs)
                print "You might want to consider updating (Dependencies not checked!)"
            else:
                print "{all_ok}The Package {all_ok}{pak}{reset} is OK, ready to rumble.".format(**kwargs)
    else:
        print "you either did leave the 'your_versions.txt' file empty or it doesn't exist. Please create one or fill it with content"

if __name__ == "__main__":
    main()
