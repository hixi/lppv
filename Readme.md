Disclaimer
==========

You are probably better off by using yolk directly...

Errors most likely, it's just a simple, erh.., programm is said too much, it's really just a simple hack for the lazy!

Installation
============

If you are proficient with python, you might want to skip this
part, it lists only one way of doing it (virtualenv).

Download this from bitbucket, either via cloning (a) or by getting a tarball/"zipball":

Step 1
------

a) git clone ssh://git@bitbucket.org/hixi/lppv.git lppv

instead of using lppv you might want to choose your own directory

go to step 2

b) create a new directory, ie. lppv

>cd lppv/

>wget https://bitbucket.org/hixi/lppv/get/master.zip

>unzip master.zip

got to step 2

Step 2
------

in your directory, create a new virtualenv:

> virtualenv .

use pip to install the dependencies

> . bin/activate

> pip install -r requirements.txt

You are now ready to go!

Usage
-----

Insert your packages list with the format <Package>==<Version> into your_versions.txt, a package per line:

For example:

>File: your_versions.txt

>Django==1.3.1

>django-cms==1.4.0

>django-filer==0.8

Then run ./get_recent_versions.py and you get listed, which packages could possibly need an update and which ones are up to date.
